# Nhatduong Javascript Practice2 Tasks Lists Manager

# nhatduong-javascript-practice2-tasks-lists-manager

## DEVELOPMENT TEAM

- Lead - Thong Nguyen.
- Tech guy - Nhat Duong.

## PLAN

- Plan on gitlab [Board](https://gitlab.com/walkchicken/nhatduong-javascript-practice2-tasks-lists-manager/-/boards/4072381).
- Plan on doc [Internship JavaScript Practice 2 Tasks Lists Manager](https://docs.google.com/document/d/1nvHocU4ubg--nE1TFqt-MnldhBEYTPS00BElVPp3BPg/edit).

## PROJECT TOOLS

- Parcel.
- VScode.

## TECHNICAL STACK

- HTML5.
- CSS3.
- Sass.
- JavaScript
- JSON-server

## ENVIRONMENT

- Develop environment [nhat-duong-tasks-lists-manager](https://nhat-duong-tasks-lists-manager.herokuapp.com/).

## REFERENCES

- [JSON-server](https://viblo.asia/p/xay-dung-json-server-voi-thu-vien-json-server-07LKX7O45V4).
- [Drag and drop](https://viblo.asia/p/keo-tha-dragable-trong-js-va-cach-dung-m68Z0OOzKkG).
- [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch).

## ISSUES

- Issues and how to solve them

## SETUP & RUN

```
git clone https://gitlab.com/walkchicken/nhatduong-javascript-practice2-tasks-lists-manager
cd javascript-practice-1-book-manager
yarn install
yarn build
json-server --watch ./server/db.json
yarn start
output: http://localhost:1234
```
