/**
 * //* test api send request
 */

const userAPI = "http://localhost:3000/users";
const listAPI = "http://localhost:3000/lists";
const taskAPI = "http://localhost:3000/tasks";
const labelAPI = "http://localhost:3000/labels";
/**
 * 
 * @returns //* apis send request return array data users, lists, tasks, labels
 * //* example : 0: {id: 1, title: 'To Do'}
                1: {id: 2, title: 'In Progress'}
                2: {id: 3, title: 'Done'}
 */

async function getUser() {
    const response = await fetch(userAPI);
    try {
        const users = await response.json();
        /**
         * @returns //* convert data to json
         */
        return users;
    } catch (error) {
        throw Error(error);
    }
}

getUser().then(users => {
    users;  
    /** //! fetch users
    */
    console.log(users.find(({ username }) => username === "thongnguyen"));
});

async function getList() {
    const response = await fetch(listAPI);
    try {
        const lists = await response.json();
        return lists;
    } catch (error) {
        throw Error(error);
    }
}

getList().then(lists => {
    lists; 
    /** //! fetch lists
    */
    console.log(lists);
});
