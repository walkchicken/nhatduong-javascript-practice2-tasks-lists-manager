class LoginView {
    constructor() {
        this.btnLogin = document.getElementById("submit");
        /**
         * @returns //* selector button id="submit".
         */
    }
    
    bindLogin = (fn) => {
        /**
         * //* add event click for button
         */
        this.btnLogin.addEventListener("click", (e) => {
            e.preventDefault();
            const username = document.getElementById("username").value;
            const password = document.getElementById("password").value;
            /**
             * @returns //* get value from input username and password.
             */
            console.log(username,password);
            fn(username,password);
            /**
             * @returns //* create fn with params is value.
             */
        });
    }
}

export default LoginView;
