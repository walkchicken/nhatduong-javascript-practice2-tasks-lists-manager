class ListController {
    constructor(listModel, listView) {
        this.listModel = listModel;
        this.listView = listView;
    }
}

export default ListController;
