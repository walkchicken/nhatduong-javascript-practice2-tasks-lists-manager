class LoginController {
    constructor(loginModel, loginView) {
        this.loginModel = loginModel;
        this.loginView = loginView;
    }

    viewHandleLogin() {
        this.loginView.bindLogin(this.handleLoginUser.bind(this));
        /**
         * @returns //! using bind(this)to specifically define this.loginView.
         */
    }
     /**
     * @returns //* create fn connect to view from controller
     */

    /**
     * 
     * @param {const} username 
     * @param {const} password 
     */
    handleLoginUser(username, password) {
        this.loginModel.login(username, password);
    }
    /**
     * @returns //* pass params and call to login function on model
     */
}

export default LoginController;
