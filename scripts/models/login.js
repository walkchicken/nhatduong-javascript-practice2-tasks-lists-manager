class LoginModel {
    constructor() {}

    async login(username, password) {
        const response = await fetch("http://localhost:3000/users");
        /**
         * @returns //* fetch api to call json-server
         */
        try {
            const users = await response.json();
            /**
             * @returns //* convert data to json 
             */
            const validateName = username;
            const validatePassword = password;
            /**
             * @returns //* save username and pass in 2 variable.
             */
            if(users.find(({ username }) => username === validateName) && users.find(({ pass }) => pass === validatePassword)) {
                window.location.href = "http://localhost:1234/index.html";
                /**
                 * @returns //* open index page if correct
                 */
            }
            /**
             * @returns //?compare the two stored variables with the data called out in json-server
             */
        } catch (error) {
            throw Error(error);
        }
    }
}

export default LoginModel;
