import ListController from "../controllers/list";
import ListModel from "../models/list";
import ListView from "../views/list";
/**
 * @returns //* import class from controllers, models and views folder.
 */

const listModel = new ListModel();
const listView = new ListView();
const listController = new ListController(listModel, listView);

