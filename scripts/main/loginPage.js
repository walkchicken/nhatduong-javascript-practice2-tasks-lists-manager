import LoginController from "../controllers/login";
import LoginModel from "../models/login";
import LoginView from "../views/login";
/**
 * @returns //* import class from controllers, models and views folder.
 */


const loginModel = new LoginModel();
const loginView = new LoginView();
const loginController = new LoginController(loginModel, loginView);

loginController.viewHandleLogin();
/**
 * @returns //* call fn viewHandLogin() from loginController.
 */
